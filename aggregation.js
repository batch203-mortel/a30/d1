// [Section] MongoDB Aggregation
/*
	-Used to generate, manipulate and perform operations to create filtered results that helps us to analyzed the data.

*/
	
		// Using the aggregate method:
		/*
			-The "$match"
			- Syntax:
				{$match: {field: value}}
			- The "$group"
			-Syntax:
				{$group: {_id: "value", fieldResult: "valueResult"}}

		*/

		db.fruits.aggregate([
				{$match: {onSale: true}},
				{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
			]);


		// Field projection with aggregation
		/*
			-The $project can be used when aggregating data to include/exclude fields from the returned result.
			-Syntax:
			{$project: {field: 1/0}}

		*/

		db.fruits.aggregate([
				{$match: {onSale: true}},
				{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
				{$project: {_id: 0}}
			]);

// Sorting Aggregated Results
/*
	-The "$sort" can be used to change the order of the aggregated result
	syntax:
	{$sort:{field:1}}
*/

		db.fruits.aggregate([
				{$match: {onSale: true}},
				{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
				{$sort: {total:0}}

			]);

		//  Aggregating results based on array fields
		/*
			- The "$unwind" deconstructs an array field  from a collection/field with an array value to put a result
			-Syntax:
			-{unwind: field}

		*/
		db.fruits.aggregate([
				{$unwind: "$origin"}
			]);
		db.fruits.aggregate([
				{$unwind: "$origin"},
				{$group: {_id: "$origin"}, kinds:{$sum:1}}
			]);

		// [Section] Other Aggregate stages

		// $count all yellow fruits
		db.fruits.aggregate([
				{$match: {color: "Yellow"}}
			])

		db.fruits.aggregate([
				{$match: {color: "Yellow"}},
				{$count: "Yellow Fruits"}
			])

		// $avg gets the average value of stock
			db.fruits.aggregate([
					{$match: {color: "Yellow"}},
					{$group: {_id:"$color", yellow_fruits_stock: {$avg: "$stock"}}}
				])

		// $min and $max
			db.fruits.aggregate([
					{$match: {color: "Yellow"}},
					{$group: {_id:"$color", yellow_fruits_stock: {$min: "$stock"}}}
				])
